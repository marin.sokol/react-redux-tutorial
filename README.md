# REACT / REDUX STATE MANAGEMENT TUTORIAL

This is first part of tutorial. We are going to build simple React app using only React state and stateful components. Stateful component is React component that contains app logic and important parts of application data like lists of items, users, ...

We won't be using routers, because we want to make this tutorial as simple as possible so everybody can try it. We will be using tab menu to simulate router and router behavior.

We are going to use Ant.design component so we don't need to write our own css, plus Ant has some realy nice component easy to use with very simple css modification if you need.

## Application
We are building application that gets data about characters, starships, vehicles and planets from Star Wars. We are going to build tabs containing information about each category from Star Wars and favorites tab. Favorites tab will contain all data someone favored. Also we are going to build modal containing details about character, starship, ... that user clicked.

Favorites will contain only IDs of selected data and we will that get data from original array by that ID.

All data will be saved in root component so we can get any data we need, such as information about our favourite characters, starships, vehicles and planets from IDs that we save.

## Requirements
* node `^5.0.0`
* yarn `^0.23.0` or npm `^3.0.0`
* basic React knowlage

## Starting tutorial
1. Clone this repository
    ```bash
    $ git clone https://gitlab.com/marin.sokol/react-redux-tutorial.git <my-project-name>
    $ cd <my-project-name>
    ```
1. Install the project dependencies
    ```bash
    $ yarn  # Install project dependencies (or `npm install`)
    ```
1. Run project (page will reload on any change since project is build with [create-react-app](https://github.com/facebookincubator/create-react-app))
    ```bash
    $ yarn start # Install project dependencies (or `npm start`)
    ```
    You can ignore any warnings for now.
1. Open project in editor of your choise (VS Code, Sublime, Atom, WebStorm,...)
1. Open src folder, whole project is build from src folder. Files important for our project are App.js and components folder.

## Tutorial
This is 2 part tutorial. First part is React state management and second Redux state management. You don't need to do both parts, you can do only one interesting for you. 

If you want to do only React state management, you should switch to `react-state` branch.
```bash
$ git checkout react-state
$ yarn start
```

If you want to do only Redux state management, you should switch to `redux-state` branch.
```bash
$ git checkout redux-state
$ yarn start
```

If you want to do both, you should stay on this branch. There are 2 branches with solutions.

    `react-state-solution` -> React state management solution 
    `redux-state-solution` -> Redux state management solution 

## Steps 

1. Lets start by loading Star Wars characters to our People compnent. You can see that People component and all other compnents are already imported and placed into App.js
1. We are going to build table of Star Wars characters with pagination. We are going to render 10 by 10 characters.
1. Open `src/components/App.js`.
1. Since `People` component is first rendered, add API request to get initial data to componentDidMount method. We already imported [axios](https://github.com/mzabriskie/axios) and `config` containing api url for API requests.
1. Add API request to get initial data to componentDidMount method. We already imported [axios](https://github.com/mzabriskie/axios) and `config` containing api url for API requests.
1. Response from servers should contain number of characters and array of first 10 characters.
1. There is no ID in character object. We will need ID later for adding to Favorites. So, we are going to build helper function what will get ID from url (url contains id that will be send to api)
1. At top of `App.js` we already imported helper function. Now you need to open `src/helpers/index.js` and build function. Function will return array with all object set to function. Each object will contain all original properties + ID property.
1. You should see all 10 characters in table with all field full.
1. It is time to build pagination. Our Ant pagination will return page number what is clicked, but we need to load people in `App`. So you are going to build function in `App` that will be sent to all other components and recieve page and data category to load. This is so you don't need different function for every single category.
1. Build function mentioned in step before and console.log page and data category in `App` after new page is clicked in `People`
1. After you are done. If you click new page, you should have something like this in browser console
    ```
    Page: 2
    Catergory: people
    ```
1. You need to build API request for that page, but first you need to check whether you already have data loaded. Because you don't want to load data more time that is needed.
1. Next step is to build `Details`. We are using [Ant modal](https://ant.design/components/modal/) as details page. In `App` state there is modal object. You wont save whole object from pressed table row, because you don't want to have same data in different places in your app. So, you will save id and category of pressed row.
1. Open `App` add similar function like function for pagination. This function will recieve id and category of object and save it to modal object in state, than pass that function to `People`.
1. Open `People` and implement function to handle press on row to send id and category to `App`
1. Return to `App`. Now, you need to add props for `Details`.
    * `visible` -> bool that will be flag to show modal; already in modal object in state
    * `category` -> category of object that will be rendered; already in modal object in state
    * `data` -> object that is going to rendered; you will need to find that object in state
1. Open `Details`. Change title to be ``` category - name ``` then modal is showen and replace ``` content column ``` with couple properties from object that you like.
1. In `Details`, you can see planet in state. Add API request to get planet name and render it in ``` content column ```.
1. Return to `App`, create function that will close modal. This function don't recieve any data. It will only be triggered on click to close. That send that function to `Details` as prop.
1. Open `Details` and trigger function to close on click on any button. There are 2 buttons; 'OK' and 'Cancle'; but you don't to implement different function because you only want to close modal.
1. Now, you will implement adding data to favourites. As you can see in `App` state favourites object. You will add only ID to category array so you don't have duplicate of data. You should console.log favourites object in render to check if you implement state change correctly.
1. Open `App` and implement function that will change state when it gets id and category. Then pass it down to People component.
1. Open `People` add click handle on star icon. You will need to pass click event and data you will get from render function. You need click event because you need to stop modal from opening. Function needs to send ID and category to `App`.
1. You will add dark star for people that are in favourites. You will need to open `src/helpers/index.js` and implement `getDataWithFavourite` function. You will built it similar to `getDataWithId` with difference being that you will add `favourite` prop. That prop will be bool; true if object is favourited. You will send category of data and array of that category ID from favourites.
1. You will call `getDataWithFavourite` before sending people array to `People`.
1. Now, you will get object with `favourite` prop in icon render. If `favourite` is true, you will render `star`, and if not, `star-o`. Both icon should have onClick method on them.
1. Now, do same thing for `Details`. You will find `getDetailsWithFavourite` in `src/helpers/index.js`. That function will return single object with favourite prop for details modal.
1. Lets build `Favourites`. There is `getFavourites` function in `src/helpers/index.js`. You should send state to it and return object with all object that are favourited and pass them to `Favourites`. You should add `category` prop to each object so You can know from which category You should remove favourite later.
1. Add function in `App` that will be called when you want to remove object and send it to `Favourites`. Add click handle in delete icon and send clicked object to function passed from `App`.
1. When you got that working, repeat process for all categories. You shoud need to make changes to `Details` and `Favourites`
1. You should add change handle on `Tabs` and load new data if clicked category doesn't have any data already loaded.
