/**
 * Function that will go thru array and get id from url for every object.
 * You should use map function because it returns new array
 * You can get ID from url and save it variable.
 * Then, assign that variable to id property by using spread operator on origin object.
 */
export const getDataWithId = (data) => { }

/**
 * Function will recieve data array with data from some category and array of category IDs
 * Function that will go thru array and find does ID exists in favourites data.
 * You should use map function because it returns new array
 */
export const getDataWithFavourites = (data, favourites) => { }

/**
* Function will recieve state object
* You will find object for selected data and find does ID exists in favourites data.
* You should return object with added favourite prop
*/
export const getDetailWithFavourites = (state) => { }

/**
 * Function will recieve state object.
 * You need to create array of objects that are favourited. So you should take favourites object and find all object by ID and category
 */
export const getFavourites = (state) => { }
