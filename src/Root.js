import React, { PureComponent } from 'react';
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import App from './App';

export default class extends PureComponent {
  static displayName = 'Root'

  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <LocaleProvider locale={enUS}>
        <App />
      </LocaleProvider>
    )
  }
}
