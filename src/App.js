import React, { PureComponent } from 'react';
import axios from 'axios';
import {
  Tabs,
  Layout,
  Col,
} from 'antd';
import People from './components/People';
import Starship from './components/Starship';
import Vehicles from './components/Vehicles';
import Planets from './components/Planets';
import Favourites from './components/Favourites';
import Details from './components/Details';
import config from './config';
import {
  getDataWithId,
  getDataWithFavourites,
  getDetailWithFavourites,
  getFavourites,
} from './helpers';

const { TabPane } = Tabs;
const { Header, Content } = Layout;

export default class extends PureComponent {
  static displayName = 'App'

  /**
   * State contain all data we need.
   * * data = data that is going to be rendered
   * * count = number of data for pagination
   * * loading = bool variable that we will use to change what we are loading next set of people data
   */
  state = {
    people: {
      data: [],
      count: 0,
      loading: true,
    },
    starships: {
      data: [],
      count: 0,
      loading: true,
    },
    vehicles: {
      data: [],
      count: 0,
      loading: true,
    },
    planets: {
      data: [],
      count: 0,
      loading: true,
    },
    favorites: {
      data: {
        people: [],
        starships: [],
        vehicles: [],
        planets: [],
      },
      count: 0,
      loading: true,
    },
    modal: {
      visible: false,
      id: '-1',
      category: null,
    }
  }

  /**
   * Loading people for first time.
   * componentDidMount method triggers when component is visible on page
   * Url we imported from config doesn't contain full url, only main path.
   * You have to add /people at end of url.
   */
  componentDidMount() { }

  /**
   * Recives data about next page to load and loads new data if needed.
   * You should recieve two parameters
   * * page = page that is return by ant
   * * category = hardcoded category of data for each component; 'people' for People component, 'starships' for Starship compnent
   */
  handleNewPage = (page, category) => { }

  /**
   * Recives data object id and category so you can render details
   * You should recieve two parameters
   * * object id = id from pressed row in some component
   * * category = hardcoded category of data for each component; 'people' for People component, 'starships' for Starship compnent
   */
  handleOpenDetails = (id, category) => { }

  /**
   * You will trigger this callback when you click OK or Cancle
   * You will set modal object to initial modal object
   */
  handleCloseDetails = () => { }

  /**
   * Recives data about object id and category so you can add or remove from favourites
   * If id exists in favourites, you should remove it and add if it doesn't exist
   * You should recieve two parameters
   * * object id = id data you want to add or remove from favourites
   * * category = hardcoded category of data for each component; 'people' for People component, 'starships' for Starship compnent
   * You will change count prop as you toggle objects
   */
  handleToggleFavourites = (id, category) => { }

  /**
   * Recives object that needs to be removed.
   * You will use category and id prop from object.
   */
  handleRemoveFavourite = (data) => { }

  /**
   * Recives key from clicked tab. Key is category that is currently visible.
   * Check is category data empty and load new data
   */
  handleChange = (category) => { }

  /**
   * We are sending specific data to child components via props
   */
  render() {
    const { people, starships, vehicles, planets, favorites } = this.state;

    return (
      <Layout>
        <Header>
          <Col md={12} xs={12}>
            <img src="/images/blueprint.png" alt="REACT / REDUX beginner tutorial" />
            REACT / REDUX beginner tutorial
            </Col>
          <Col md={12} xs={12}>
            Powered by
              <a href="https://swapi.co">
              <b>SWAPI</b>
            </a>
          </Col>
        </Header>
          <Content>
            <Tabs defaultActiveKey="people">
              <TabPane tab="People" key="people">
                <People
                  people={people.data}
                  count={people.count}
                  loading={people.loading}
                />
              </TabPane>
              <TabPane tab="Starship" key="starships">
                <Starship />
              </TabPane>
              <TabPane tab="Vehicles" key="vehicles">
                <Vehicles />
              </TabPane>
              <TabPane tab="Planets" key="planets">
                <Planets />
              </TabPane>
              <TabPane tab="Favourites" key="favourites">
                <Favourites
                  data={[]}
                  count={0}
                  loading={false}
                />
              </TabPane>
            </Tabs>
            {/**
            * Same component will be render for all category details.
            * You can calculate date by getting part of state you need with category
            * and find object with find method on array from JS.
            * You should send null if object is not finded.
            */}
            <Details
              visible={false}
              data={null}
              category={null}
            />
          </Content>
      </Layout>
        );
  }
}
