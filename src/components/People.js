import React, { PureComponent } from 'react';
import { Table, Icon } from 'antd';

export default class extends PureComponent {
  static displayName = 'People'

  /**
   * Const columns defines how to render people to Ant table.
   * You can change that is shown by adding and removing objects from array.
   * Object has to have 3 properties:
   * * title = show in table header
   * * dataIndex = property from object you are rendering
   * * key = property you can choose, but has to be specific to that object
   *
   * Last object constains render because we are rendering star button so we can add it to favorites.
   * Render returns function with data foreach object in array you want to render.
   */
  columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Age',
      dataIndex: 'birth_year',
      key: 'birth_year',
    },
    {
      title: 'Favorite',
      key: 'action',
      render: (data) => {
        return (
          <Icon
            type="star-o"
          />
        )
      },
    },
  ];

  /**
   * Handles press on new page.
   * This is function from Ant and return number of press page.
   * Send needed data to App with function you passed in props.
   * * page = page that is return by ant
   * * category = hardcoded category for each component; 'people' for People component
   */
  handleChange = (page) => { }

  /**
   * Handles click on row.
   * This function returns object with data from clicked row.
   * Send needed data to App with function you passed in props.
   * * id = will be in object sent in event callback
   * * category = you will hardcode it for each component; 'people' from this compnent
   */
  handleRowClick = (record) => { }

  /**
   * Handles click on star icon in row.
   * This function returns click event and object with data from clicked row.
   * You should stop click event so details modal will not open.
   * Send needed data to App with function you passed in props.
   * * id = object id of selected object
   * * category = you will hardcode it for each component; 'people' from this compnent
   */
  handleFavouriteClick = (e, data) => { }

  render() {
    const { people, count, loading } = this.props;

    return (
      <div>
        <Table
          loading={loading}
          dataSource={people}
          columns={this.columns}
          pagination={{
            simple: true,
            pageSize: 10,
            total: count,
            onChange: this.handleChange
          }}
          onRowClick={this.handleRowClick}
        />
      </div>
    );
  }
}
