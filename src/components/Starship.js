import React, { PureComponent } from 'react';
import { Table, Icon } from 'antd';

export default class extends PureComponent {
  static displayName = 'Starships'

  columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Model',
      dataIndex: 'model',
      key: 'model',
    },
    {
      title: 'Cost',
      dataIndex: 'cost_in_credits',
      key: 'cost_in_credits',
    },
    {
      title: 'Favorite',
      key: 'action',
      render: (data) => (
        <Icon
          type="star-o"
        />
      ),
    },
  ]

  render() {
    const { starships, count, loading } = this.props;

    return (
      <div>
        <Table
          loading={loading}
          dataSource={starships}
          columns={this.columns}
          pagination={{
            simple: true,
            pageSize: 10,
            total: count,
            onChange: this.handleChange
          }}
          onRowClick={this.handleRowClick}
        />
      </div>
    );
  }
}
