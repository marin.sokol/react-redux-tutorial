import React, { PureComponent } from 'react';
import { Table, Icon } from 'antd';

export default class extends PureComponent {
  static displayName = 'Favourites'

  columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Age',
      dataIndex: 'birth_year',
      key: 'birth_year',
    },
    {
      title: 'Model',
      dataIndex: 'model',
      key: 'model',
    },
    {
      title: 'Cost',
      dataIndex: 'cost_in_credits',
      key: 'cost_in_credits',
    },
    {
      title: 'Gravity',
      dataIndex: 'gravity',
      key: 'gravity',
    },
    {
      title: 'Climate',
      dataIndex: 'climate',
      key: 'climate',
    },
    {
      title: 'Delete',
      key: 'action',
      render: (text, record) => (
        <Icon type="delete" />
      ),
    },
  ];

  /**
   * Recives data about clicked objects and sends it to App component with function sent as props.
   * Object will be removed from favourites
   */
  handleClick = (data) => { }

  render() {
    const { data, count } = this.props;

    return (
      <div>
        <Table
          dataSource={data}
          columns={this.columns}
          pagination={{
            pageSize: 10,
            total: count,
            onChange: this.handleChange
          }}
        />
      </div>
    );
  }
}
