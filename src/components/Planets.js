import React, { PureComponent } from 'react';
import { Table, Icon } from 'antd';

export default class extends PureComponent {
  static displayName = 'Planets'

  columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Gravity',
      dataIndex: 'gravity',
      key: 'gravity',
    },
    {
      title: 'Climate',
      dataIndex: 'climate',
      key: 'climate',
    },
    {
      title: 'Favourite',
      key: 'action',
      render: (data) => (
        <Icon
          type="star-o"
        />
      ),
    },
  ]

  render() {
    const { planets, count, loading } = this.props;

    return (
      <div>
        <Table
          loading={loading}
          dataSource={planets}
          columns={this.columns}
          pagination={{
            simple: true,
            pageSize: 10,
            total: count,
            onChange: this.handleChange
          }}
          onRowClick={this.handleRowClick}
        />
      </div>
    );
  }
}
