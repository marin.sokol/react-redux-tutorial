import React, { PureComponent } from 'react';
import axios from 'axios';
import {
  Modal,
  Row,
  Col,
  Icon,
} from 'antd';

export default class extends PureComponent {
  static displayName = 'Details'

  state = {
    planet: ''
  }

  /**
   * You are going to use componentWillUpdate function because that will be called every time new props are sent to compnent.
   * You are using this function because this compnent will rerender everytime you click on row.
   * You should check is modal going to be visible and does data has property with planet url.
   */
  componentWillUpdate(nextProps) { }

  /**
   * Function will be called on every click on button or space around modal.
   * You should trigger function passed from props to close modal, without any data.
   */
  handleClose = () => { }

  /**
   * Handles click on star icon in row.
   * This function returns click event and object with data from clicked row.
   * You should stop click event so details modal will not open.
   * Send needed data to App with function you passed in props.
   * * id = object id of selected object
   * * category = you will get it as prop from App
   */
  handleFavouriteClick = () => { }

  render() {
    const { visible, category, data } = this.props;
    const { planet } = this.state;

    return (
      <Modal
        title={'title'}
        visible={visible}
        onOk={this.handleClose}
        onCancel={this.handleClose}
      >
        <Row>
          <Col md={12} xs={12}>
            Content
          </Col>
          <Col md={12} xs={12} style={{ textAlign: 'center', fontSize: '25px' }}>
            <Icon type="star-o" />
          </Col>
        </Row>
      </Modal>
    );
  }
}
